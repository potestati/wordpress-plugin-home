<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wordpress1');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '1X1utA:UgkK@#+ki;@N?4ZxBxu[&H|]umSFy[8EW=M`+23G*V3C=2[s1b%Hk?K%E');
define('SECURE_AUTH_KEY',  'p3b/+WBr7xm(;AXoOZDm&yf4Hmo5)F2im<M!{2PFlRieYr&s?xJc|7JJRR /kr4h');
define('LOGGED_IN_KEY',    '?HgM#8r@^HvbM]6:i}1V1=WvEK#qnrnjvif{jlit}7:gidY(VyYIe@.6A(~iB]X!');
define('NONCE_KEY',        'xX&`XN: ,z^x{Os6pfaEpX~6<`T{I1#Z2(F%.%?jRuOK~!)?p~8T*I,.6oFXA;M6');
define('AUTH_SALT',        '|cpZ5.8&}q[?B~ 0l&})ynSB{;pEPol!EQB%cI:D7/7:zQr/D-N>(j}f3Y(e_5%T');
define('SECURE_AUTH_SALT', 'lN+IuqzEy8%#gEuD~@G{iD_&8_gtnZ<PSqD]fKiRfN30QY=Vl8Q+PtWkR rM9#N8');
define('LOGGED_IN_SALT',   '3`<znrLmEUs*GEg<k0EAp6(HsmW5aMk]p!IsQGtXcS+fwsh.?Eh}~cW4p:TihveG');
define('NONCE_SALT',       'X)cD+Fz!-TN>HB1,0NA wzsQlm`~(w h8nq:bCA#b4K]!Bu3ZtP,kEt(4l.0yA:3');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
