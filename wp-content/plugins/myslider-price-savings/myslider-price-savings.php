<?php
/*
  Plugin Name: Myslider Price Show Savings
  Plugin URI: https://github.com/dragus79/plugin-wp
  Description: Plugin for displaying jQuery UI Slider.
  Version: 1.0.0
  Author: Dragana Poznan
  Author URI: https://executive-digital.com/
  License: GPLv2 or later
  Text Domain: myslider-price
 */

class MysliderPrice {

    function __construct() {
        add_action('init', array($this, 'custom_post_type'));
//        add_action('widgets_init', 'register_my_widget');
    }

    function register() {
// add_action('admin_enqueue_scripts', array($this, 'enqueue'));
        $this->enqueue();
    }

    function activate() {
// generated a CPT
        $this->custom_post_type();
// flush rewrite rules
        flush_rewrite_rules();
    }

    function deactivate() {
// flush rewrite rules
        flush_rewrite_rules();
    }

    function custom_post_type() {
        $labels = array(
            'name' => 'Posts with Fee Slider',
            'singular_name' => 'Post with Fee Slider',
            'menu_name' => 'Posts with Fee Slider',
            'name_admin_bar' => 'Posts with Fee Slider',
            'add_new' => 'Add New',
            'add_new_item' => 'Add New Price',
            'new_item' => 'New Fee Slider',
            'edit_item' => 'Edit Fee Slider',
            'view_item' => 'View Fee Slider',
            'all_items' => 'All Fee Slider',
            'search_items' => 'Search Fee Slider',
            'parent_item_colon' => 'Parent Fee Slider',
            'not_found' => 'No Fee Slider Found',
            'not_found_in_trash' => 'No Fee Slider Found in Trash'
        );

        $args = array(
            'labels' => $labels,
            'public' => true,
            'exclude_from_search' => false,
            'publicly_queryable' => true,
            'show_ui' => true,
            'show_in_nav_menus' => true,
            'show_in_menu' => true,
            'show_in_admin_bar' => true,
            'menu_position' => 5,
            'menu_icon' => 'dashicons-admin-appearance',
            'capability_type' => 'post',
            'hierarchical' => false,
            'supports' => array('title', 'editor', 'custom-fields'),
            'has_archive' => true,
            'rewrite' => array('slug' => 'feeslider'),
            'query_var' => true
        );

        register_post_type('fee_slider', $args);
// create_taxonomies();
// register_post_type('book', ['public' => true, 'label' => 'Books']);
    }

    function enqueue() {
// enqueue all our scripts
        wp_enqueue_style('mypluginstyle', plugins_url('/css/main.css', __FILE__));
        wp_enqueue_style('mypluginstyle1', plugins_url('/css/jquery-ui.css', __FILE__));
        wp_enqueue_script('mypluginscript', plugins_url('/js/main.js', __FILE__), array('jquery'), false, true);
        wp_enqueue_script('mypluginscript1', plugins_url('/js/jquery-ui.js', __FILE__));
        wp_enqueue_script('mypluginscript2flat', plugins_url('/js/jquery.ui.touch-punch.min.js', __FILE__));
    }


    function render($atts = []) {
        // atts je array sa argumentima iz shortcode taga
//        var_dump($atts);

//        $maxprice = $atts[maxprice];
        $maxprice = 1000000;
//        $minprice = $atts[minprice];
        $minprice = 100000;
        $price = $atts[price];
//        $price = 3333;
        $htmlparams = '<div hidden id="fee-slider-params" data-shortcode-price=' . $price . ' data-shortcode-minprice=' . $minprice . ' data-shortcode-maxprice=' . $maxprice . '></div>';
        $html = file_get_contents(plugins_url('/fee-template/index.html', __FILE__));
        echo $htmlparams; // oavj div sluzi samo da javascriptu proslkedimo parametre
        echo $html;
    }

}

//class Fee_Slider_Widget extends WP_Widget {}
//class Foo_Widget extends WP_Widget {

class Fee_Slider_Widget extends WP_Widget {

    /**
     * Register widget with WordPress.
     */
    function __construct() {
        parent::__construct(
                'fee_widget', // Base ID
                esc_html__('Fee Slider Widget', 'text_domain'), // Name
                array('description' => esc_html__('A Fee Widget', 'text_domain'),) // Args
        );
    }

    /**
     * Front-end display of widget.
     *
     * @see WP_Widget::widget()
     *
     * @param array $args     Widget arguments.
     * @param array $instance Saved values from database.
     */
    public function widget($args, $instance) {
        // ovo je za render widgeta
        echo $args['before_widget'];
        if (!empty($instance['price'])) {
            echo $args['before_title'] . apply_filters('widget_title', $instance['price']) . $args['after_title'];
        }
        echo esc_html__('Hello, CAO World! ', 'text_domain');
        // echo $args['after_widget']; // ??? zbunjujuce. verovatno treba izbegavati i koristiti $instance
        // treba da iz polja PRICE uzme vrednost u promenjivu
        // i treba da je stavi u atribut u parent div od slidera..
        // i da se dobro ucita css i javascript od slidera.

        $neki_price = $instance['price'];
        $html = file_get_contents(plugins_url('/fee-template/index.html', __FILE__));
        echo '<div id="parent-fee-slider" data-field-price=';
        echo $neki_price;
        echo '">';
        echo $html; // html kod iz templatea od slidera
        $html_slider_script = file_get_contents(plugins_url('/js/main.js', __FILE__));
        echo '<script>' . $html_slider_script . '</script>';
        echo '</div>';
    }

    /**
     * Back-end widget form.
     *
     * @see WP_Widget::form()
     *
     * @param array $instance Previously saved values from database.
     */
    public function form($instance) {
        $widgetPrice = !empty($instance['price']) ? $instance['price'] : esc_html__(' ', 'text_domain');
        ?>
        <p>
            <label for="<?php echo esc_attr($this->get_field_id('price')); ?>"><?php esc_attr_e('Price:', 'text_domain'); ?></label> 
            <input class="widefat" id="<?php echo esc_attr($this->get_field_id('price')); ?>" name="<?php echo esc_attr($this->get_field_name('price')); ?>" type="text" value="<?php echo esc_attr($widgetPrice); ?>">
        </p>
        <?php
    }

    /**
     * Sanitize widget form values as they are saved.
     *
     * @see WP_Widget::update()
     *
     * @param array $new_instance Values just sent to be saved.
     * @param array $old_instance Previously saved values from database.
     *
     * @return array Updated safe values to be saved.
     */
    public function update($new_instance, $old_instance) {
        $instance = array();
        $instance['price'] = (!empty($new_instance['price']) ) ? strip_tags($new_instance['price']) : '';

        return $instance;
    }

}

// register Foo_Widget widget
function register_fee_widget() {
    register_widget('Fee_Slider_Widget');
}

//add_action('widgets_init', 'register_fee_widget');

function create_post_typebbbbbbb() {
    
}

function create_taxonomies() {

// Add a taxonomy like categories
    $labels = array(
        'name' => 'Types',
        'singular_name' => 'Type',
        'search_items' => 'Search Types',
        'all_items' => 'All Types',
        'parent_item' => 'Parent Type',
        'parent_item_colon' => 'Parent Type:',
        'edit_item' => 'Edit Type',
        'update_item' => 'Update Type',
        'add_new_item' => 'Add New Type',
        'new_item_name' => 'New Type Name',
        'menu_name' => 'Types',
    );

    $args = array(
        'hierarchical' => true,
        'labels' => $labels,
        'show_ui' => true,
        'show_admin_column' => true,
        'query_var' => true,
        'rewrite' => array('slug' => 'type'),
    );

    register_taxonomy('fee_slider_type', array('fee_slider'), $args);

// Add a taxonomy like tags
    $labels = array(
        'name' => 'Attributes',
        'singular_name' => 'Attribute',
        'search_items' => 'Attributes',
        'popular_items' => 'Popular Attributes',
        'all_items' => 'All Attributes',
        'parent_item' => null,
        'parent_item_colon' => null,
        'edit_item' => 'Edit Attribute',
        'update_item' => 'Update Attribute',
        'add_new_item' => 'Add New Attribute',
        'new_item_name' => 'New Attribute Name',
        'separate_items_with_commas' => 'Separate Attributes with commas',
        'add_or_remove_items' => 'Add or remove Attributes',
        'choose_from_most_used' => 'Choose from most used Attributes',
        'not_found' => 'No Attributes found',
        'menu_name' => 'Attributes',
    );

    $args = array(
        'hierarchical' => false,
        'labels' => $labels,
        'show_ui' => true,
        'show_admin_column' => true,
        'update_count_callback' => '_update_post_term_count',
        'query_var' => true,
        'rewrite' => array('slug' => 'attribute'),
    );

    register_taxonomy('fee_slider_attribute', 'fee_slider', $args);
}

if (class_exists('MysliderPrice')) {
    $mysliderprice = new MysliderPrice();
    $mysliderprice->register();
}


// shortcode [feeslider minprice="10" maxprice="1000" price="333"]
// https://wordpress.stackexchange.com/questions/61437/php-error-with-shortcode-handler-from-a-class
// https://developer.wordpress.org/plugins/shortcodes/shortcodes-with-parameters/ // primer sa atributima iz shortcode-a
add_shortcode('feeslider', [new mysliderprice, 'render']);

// activation
register_activation_hook(__FILE__, array($mysliderprice, 'activate'));

// deactivation
register_deactivation_hook(__FILE__, array($mysliderprice, 'deactivate'));


