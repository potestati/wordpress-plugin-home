<?php
/*
 * Plugin Name: Carousel
 * Plugin URI: http://127.0.0.1/
 * Description: This plugin adds carousel
 * Version: 1.0.0
 * Author: Dragana Poznan
 * Author URI: http://127.0.0.1/
 * License: GPL2
 */



add_action('admin_menu', 'carousel_admin_actions');
add_shortcode('carousel', 'carousel_admin');

function carousel_admin_actions() {
    add_options_page('carousel', 'carousel', 'manage_options', __FILE__, 'carousel_admin');
}

function carousel_admin() {

    function enqueue() {
// enqueue all our scripts
        wp_enqueue_style('mypluginstyle', plugins_url('/css/main.css', __FILE__));
        wp_enqueue_style('mypluginstyle1', plugins_url('/css/owl.carousel.css', __FILE__));
        //wp_enqueue_script('mypluginscript', plugins_url('/js/main.js', __FILE__), array('jquery'), false, true);
        wp_enqueue_script('mypluginscript1', plugins_url('/js/owl.carousel.js', __FILE__));
    }
    ?>

    <style>
        .carousel-inner.onebyone-carosel { margin: auto; width: 90%; }
        .onebyone-carosel .active.left { left: -33.33%; }
        .onebyone-carosel .active.right { left: 33.33%; }
        .onebyone-carosel .next { left: 33.33%; }
        .onebyone-carosel .prev { left: -33.33%; }
    </style>
    <div class="wrap">
        <div class="container">
            <div class="row">
                <div class="span12">
                    <div class="well">
                        <div id="myCarousel" class="carousel fdi-Carousel slide">
                            <!-- Carousel items -->
                            <div class="carousel fdi-Carousel slide" id="eventCarousel" data-interval="0">
                                <div class="carousel-inner onebyone-carosel">
                                    <div class="item active">
                                        <div class="col-md-4">
                                            <a href="#"><img src="http://placehold.it/250x250" class="img-responsive center-block"></a>
                                            <div class="text-center">1</div>
                                        </div>
                                    </div>
                                    <div class="item">
                                        <div class="col-md-4">
                                            <a href="#"><img src="http://placehold.it/250x250" class="img-responsive center-block"></a>
                                            <div class="text-center">2</div>
                                        </div>
                                    </div>
                                    <div class="item">
                                        <div class="col-md-4">
                                            <a href="#"><img src="http://placehold.it/250x250" class="img-responsive center-block"></a>
                                            <div class="text-center">3</div>
                                        </div>
                                    </div>
                                    <div class="item">
                                        <div class="col-md-4">
                                            <a href="#"><img src="http://placehold.it/250x250" class="img-responsive center-block"></a>
                                            <div class="text-center">4</div>
                                        </div>
                                    </div>
                                    <div class="item">
                                        <div class="col-md-4">
                                            <a href="#"><img src="http://placehold.it/250x250" class="img-responsive center-block"></a>
                                            <div class="text-center">5</div>
                                        </div>
                                    </div>
                                    <div class="item">
                                        <div class="col-md-4">
                                            <a href="#"><img src="http://placehold.it/250x250" class="img-responsive center-block"></a>
                                            <div class="text-center">6</div>
                                        </div>
                                    </div>
                                </div>
                                <a class="left carousel-control" href="#eventCarousel" data-slide="prev"></a>
                                <a class="right carousel-control" href="#eventCarousel" data-slide="next"></a>
                            </div>
                            <!--/carousel-inner-->
                        </div><!--/myCarousel-->
                    </div><!--/well-->
                </div>
            </div>
        </div>
    </div>
    <script>
    //        $(document).ready(function () {
    //            $('#myCarousel').carousel({
    //                interval: 10000
    //            })
    //            $('.fdi-Carousel .item').each(function () {
    //                var next = $(this).next();
    //                if (!next.length) {
    //                    next = $(this).siblings(':first');
    //                }
    //                next.children(':first-child').clone().appendTo($(this));
    //
    //                if (next.next().length > 0) {
    //                    next.next().children(':first-child').clone().appendTo($(this));
    //                } else {
    //                    $(this).siblings(':first').children(':first-child').clone().appendTo($(this));
    //                }
    //            });
    //        });
    </script>

    <script>
        $(document).ready(function () {
            $('#myCarousel').carousel({
                interval: 10000
            })
            $('.fdi-Carousel .item').each(function () {
                var next = $(this).next();
                if (!next.length) {
                    next = $(this).siblings(':first');
                }
                next.children(':first-child').clone().appendTo($(this));

                if (next.next().length > 0) {
                    next.next().children(':first-child').clone().appendTo($(this));
                } else {
                    $(this).siblings(':first').children(':first-child').clone().appendTo($(this));
                }
            });
        });
    </script>
    <?php
}
?>
<script>
    $(document).ready(function () {
        $('#myCarousel').carousel({
            interval: 10000
        })
        $('.fdi-Carousel .item').each(function () {
            var next = $(this).next();
            if (!next.length) {
                next = $(this).siblings(':first');
            }
            next.children(':first-child').clone().appendTo($(this));

            if (next.next().length > 0) {
                next.next().children(':first-child').clone().appendTo($(this));
            } else {
                $(this).siblings(':first').children(':first-child').clone().appendTo($(this));
            }
        });
    });
</script>
