<?php
/*
  Plugin Name: MyCarousel
  Plugin URI: http://127.0.0.1/
  Description: Creates a simple carousel
  Version: 0
  Author: Dragana Poznan
  Author URI: http://127.0.0.1/
  License: MIT
 */

//including js library
wp_enqueue_script('mycarouselowl', plugin_dir_url(__FILE__) . 'owl/owl.carousel.js', array('jquery'), false, true);
//ovaj deo dodaje funkcionalnost plugina u settings u admin deo
add_action('admin_menu', 'mycarousel_admin_actions');

//ovaj deo dodaje funkcionalnost plugina u settings u admin deo
function mycarousel_admin_actions() {
    add_options_page('MyCarousel', 'MyCarousel', 'manage_options', __FILE__, 'admin_carousel_option');
}

function admin_carousel_option() {

    //  including css file
    wp_enqueue_style('mycarouseladmin', plugin_dir_url(__FILE__) . 'css/main.css');

    echo 'hello from admin';
    ?>

    <?php
    global $wpdb;
    $args = array(
//        'order' => 'DESC',
//        'limit' => 3
    );

    $links = get_posts($args);
    /* ------------------------------------------- */
    /**
     * Get the posts by a category
     */
    $my_posts = get_posts(array('cat' => 'carousel'));
    /**
     * Initiate the output
     */
    $output = '';
    ?>
    <div class='container-mycarousel-admin'>
        <table class="widefat">
            <thead>
                <tr>
                    <th>My Carousel Post Title</th>
                    <th>Show Title</th>
                    <th>Post Image</th>
                    <th>Show Image</th>
                    <th>Post Description</th>
                    <th>Show Description</th>
                    <th>View Page</th>
                </tr>
            </thead>

            <tbody id="mycarousel-display">
                <?php
                foreach ($my_posts as $post) {

                    /** Add Thumbnail to the anchor if present
                     * Else The post title
                     * 
                     */
                    $outputView = '<a href="' . get_permalink($post->ID) . '">View Page</a>';
                    if (has_post_thumbnail($post->ID)) {
                        $outputImage = get_the_post_thumbnail($post->ID);
                        $outputTitle = $post->post_title;
                        $outputContent = $post->post_content;
                        echo '<tr>';
                        echo '<td>' . $outputTitle . '</td>';
                        ?>
                    <td>
                        <input class="carousel-check" type="checkbox" value="title-<?php echo $outputTitle; ?>" name="output-title">
                        <!--<input type="checkbox" name="type" value="4" />-->
                    </td>
                    <?php
                    echo '<td class="mycarousel-img">' . $outputImage . '</td>';
                    ?>
                    <td>
                        <input class="carousel-check" type="checkbox" value="image-<?php echo $outputTitle; ?>" name="output-img">
                    </td>
                    <?php
                    echo '<td>' . $outputContent . '</td>';
                    ?>
                    <td>
                        <input class="carousel-check" type="checkbox" value="content-<?php echo $outputTitle; ?>" name="output-content">
                    </td>
                    <?php
                    echo '<td>' . $outputView . '</td>';
                    echo '</tr>';
                } else {
                    $output .= $post->post_title;
                }
            }
            ?>
            </tbody>
        </table>
        <input id="submit" name="submit" type="submit" value="Display" class="btn btn-primary" onclick="return mycarouselDisplay();">
        <br>
        <button class="ajax-call">Add to View</button>
    </div>
    <script>

        var dataPost = '';
        (function ($) {
            $('#mycarousel-display').change(function () {
                var values = [];
                {
                    $('#mycarousel-display :checked').each(function () {
                        //if(values.indexOf($(this).val()) === -1){
                        values.push($(this).val());
                        // }
                    });
                    console.log(values);
                    dataPost = values;
                    console.log('this is dataPost: ' + dataPost);
                }
            });
        })(jQuery);

        function mycarouselDisplay()
        {
            console.log(dataPost);
        }
        (function ($) {
            $('.ajax-call').click(function () {
                console.log('hello from ajax');
                console.log(dataPost);
                $.ajax({
                    type: "post",
                    url: "",
                    data: "dataPost" + dataPost

                });

            });
        })(jQuery);

    </script>

    <?php
    $dataPost = $_POST['dataPost'];
    var_dump($dataPost);
}

// fornt part for admin

function display_carousel() {

//  including css library
    wp_enqueue_style('mycarouselstyleowl', plugin_dir_url(__FILE__) . 'owl/owl.carousel.css');


//    wp_enqueue_script( string $handle, string $src = '', array $deps = array(), string|bool|null $ver = false, bool $in_footer = false )
//            ( $handle, $src = '', $deps = array(), $ver = false, $in_footer = true ) {


    global $wpdb;
    $args = array(
//        'order' => 'DESC',
//        'limit' => 5
    );

    $links = get_posts($args);
    /* ------------------------------------------- */
    /**
     * Get the posts by a category
     */
    $my_posts = get_posts(array('cat' => 'carousel'));
    /**
     * Initiate the output
     */
    $output = '';
    /** Loop through the post */
    ?>
    <!--    <div class="center slider">-->
    <div class="partners">
        <div class="container">
            <div class="owl-carousel owl-theme">
                <?php
                foreach ($my_posts as $post) {

                    /** Add Thumbnail to the anchor if present
                     * Else The post title
                     * 
                     */
                    $output .= '<div class="item"><a href="' . get_permalink($post->ID) . '">';
                    if (has_post_thumbnail($post->ID)) {
                        $output .= get_the_post_thumbnail($post->ID);
                        $output .= $post->post_title;
                        $output .= $post->post_content;
                    } else {
                        $output .= $post->post_title;
                    }
                    /**
                     * Close the widget
                     */
                    $output .= '</a></div>';
                }
                echo $output;
                ?>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        (function ($) {
            $(document).ready(function () {


                var owl = $('.owl-carousel');
                owl.owlCarousel({
                    nav: true,
                    items: 4,
                    loop: true,
                    margin: 10,
                    autoplay: true,
                    autoplayTimeout: 1000,
                    autoplayHoverPause: true
                });
                $('.play').on('click', function () {
                    owl.trigger('play.owl.autoplay', [1000])
                })
                $('.stop').on('click', function () {
                    owl.trigger('stop.owl.autoplay')
                })


            });
        })(jQuery);
    </script>

    <?php
    /* ------------------------------------ */
}


