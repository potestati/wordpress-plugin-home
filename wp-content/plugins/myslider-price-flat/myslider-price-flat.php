<?php
/*
  Plugin Name: Myslider Price Only Flat
  Plugin URI: https://github.com/dragus79/plugin-wp
  Description: Plugin for displaying jQuery UI Slider.
  Version: 1.0.0
  Author: Dragana Poznan
  Author URI: https://executive-digital.com/
  License: GPLv2 or later
  Text Domain: myslider-price
 */

class MysliderPriceFlat {

    function __construct() {
        add_action('init', array($this, 'custom_post_type'));
//        add_action('widgets_init', 'register_my_widget');
    }

    function register() {
// add_action('admin_enqueue_scripts', array($this, 'enqueue'));
        $this->enqueue();
    }

    function activateFlat() {
// generated a CPT
        $this->custom_post_type();
// flush rewrite rules
        flush_rewrite_rules();
    }

    function deactivateFlat() {
// flush rewrite rules
        flush_rewrite_rules();
    }

    function custom_post_type() {
        $labels = array(
            'name' => 'Fee Slider Only Flat',
            'singular_name' => 'Fee Slider Only Flat',
            'menu_name' => 'Fee Slider Only Flat',
            'name_admin_bar' => 'Fee Slider Only Flat',
            'add_new' => 'Add New',
            'add_new_item' => 'Add New Price',
            'new_item' => 'New Fee Slider',
            'edit_item' => 'Edit Fee Slider',
            'view_item' => 'View Fee Slider',
            'all_items' => 'All Fee Slider Flat',
            'search_items' => 'Search Flat Fee Slider',
            'parent_item_colon' => 'Parent Flat Fee Slider',
            'not_found' => 'No Fee Flat Slider Found',
            'not_found_in_trash' => 'No Flat Fee Slider Found in Trash'
        );

        $args = array(
            'labels' => $labels,
            'public' => true,
            'exclude_from_search' => false,
            'publicly_queryable' => true,
            'show_ui' => true,
            'show_in_nav_menus' => true,
            'show_in_menu' => true,
            'show_in_admin_bar' => true,
            'menu_position' => 6,
            'menu_icon' => 'dashicons-admin-customizer',
            'capability_type' => 'post',
            'hierarchical' => false,
            'supports' => array('title', 'editor', 'custom-fields'),
            'has_archive' => true,
            'rewrite' => array('slug' => 'feesliderflat'),
            'query_var' => true
        );

        register_post_type('fee_sliderflat', $args);
// create_taxonomies();
// register_post_type('book', ['public' => true, 'label' => 'Books']);
    }

    function enqueue() {
// enqueue all our scripts
        wp_enqueue_style('mypluginstyleflat', plugins_url('/css/main.css', __FILE__));
        wp_enqueue_style('mypluginstyle1flat', plugins_url('/css/jquery-ui.css', __FILE__));
        wp_enqueue_script('mypluginscriptflat', plugins_url('/js/main.js', __FILE__), array('jquery'), false, true);
        wp_enqueue_script('mypluginscript1flat', plugins_url('/js/jquery-ui.js', __FILE__));
        wp_enqueue_script('mypluginscript2flat', plugins_url('/js/jquery.ui.touch-punch.min.js', __FILE__));
    }


    function render($attsflat = []) {
        // atts je array sa argumentima iz shortcode taga
//        var_dump($atts);

//        $maxprice = $attsflat[maxprice];
        $maxprice = 1000000;
//        $minprice = $attsflat[minprice];
        $minprice = 100000;
        $price = $attsflat[price];
//        $price = number_format($price);
//        $price = 3333;
        $htmlparams = '<div hidden id="fee-slider-params" data-shortcode-price=' . $price . ' data-shortcode-minprice=' . $minprice . ' data-shortcode-maxprice=' . $maxprice . '></div>';
        $html = file_get_contents(plugins_url('/fee-template/index.html', __FILE__));
        echo $htmlparams; // oavj div sluzi samo da javascriptu proslkedimo parametre
        echo $html;
    }

}

function create_taxonomies_flat() {

// Add a taxonomy like categories
    $labels = array(
        'name' => 'Types',
        'singular_name' => 'Type',
        'search_items' => 'Search Types',
        'all_items' => 'All Types',
        'parent_item' => 'Parent Type',
        'parent_item_colon' => 'Parent Type:',
        'edit_item' => 'Edit Type',
        'update_item' => 'Update Type',
        'add_new_item' => 'Add New Type',
        'new_item_name' => 'New Type Name',
        'menu_name' => 'Types',
    );

    $args = array(
        'hierarchical' => true,
        'labels' => $labels,
        'show_ui' => true,
        'show_admin_column' => true,
        'query_var' => true,
        'rewrite' => array('slug' => 'type'),
    );

    register_taxonomy('fee_slider_type', array('fee_sliderflat'), $args);

// Add a taxonomy like tags
    $labels = array(
        'name' => 'Attributes',
        'singular_name' => 'Attribute',
        'search_items' => 'Attributes',
        'popular_items' => 'Popular Attributes',
        'all_items' => 'All Attributes',
        'parent_item' => null,
        'parent_item_colon' => null,
        'edit_item' => 'Edit Attribute',
        'update_item' => 'Update Attribute',
        'add_new_item' => 'Add New Attribute',
        'new_item_name' => 'New Attribute Name',
        'separate_items_with_commas' => 'Separate Attributes with commas',
        'add_or_remove_items' => 'Add or remove Attributes',
        'choose_from_most_used' => 'Choose from most used Attributes',
        'not_found' => 'No Attributes found',
        'menu_name' => 'Attributes',
    );

    $args = array(
        'hierarchical' => false,
        'labels' => $labels,
        'show_ui' => true,
        'show_admin_column' => true,
        'update_count_callback' => '_update_post_term_count',
        'query_var' => true,
        'rewrite' => array('slug' => 'attribute'),
    );

    register_taxonomy('fee_slider_attribute', 'fee_sliderflat', $args);
}

if (class_exists('MysliderPriceFlat')) {
    $mysliderpriceflat = new MysliderPriceFlat();
    $mysliderpriceflat->register();
}


// shortcode [feeslider minprice="10" maxprice="1000" price="333"]
// https://wordpress.stackexchange.com/questions/61437/php-error-with-shortcode-handler-from-a-class
// https://developer.wordpress.org/plugins/shortcodes/shortcodes-with-parameters/ // primer sa atributima iz shortcode-a
add_shortcode('feesliderflat', [new mysliderpriceflat, 'render']);

// activation
register_activation_hook(__FILE__, array($mysliderpriceflat, 'activateFlat'));

// deactivation
register_deactivation_hook(__FILE__, array($mysliderpriceflat, 'deactivateFlat'));


