(function ($) {
    /* start of slider script */

    // var ocitani_value = $("#parent-fee-slider").attr("data-field-price");
    var limit = 500000;
    var ocitani_price = parseInt($("#fee-slider-params").attr("data-shortcode-price"), 10);
    var ocitani_minprice = parseInt($("#fee-slider-params").attr("data-shortcode-minprice"), 10);
    var ocitani_maxprice = parseInt($("#fee-slider-params").attr("data-shortcode-maxprice"), 10);

    console.log("ocitani value: " + ocitani_price + " " + ocitani_minprice + " " + ocitani_maxprice + " ");
    // ovaj kod za sada ne moze da se snadje (nema validator ni default vrednosti ) ukoliko prosledjeni parametri nisu ispravni.
    // mora da se proveri nakon unosa atributa u shortcode tag da li sve ispravno funkcionise.

    $('#slider').slider({
        step: 10,
        min: ocitani_minprice,
        max: ocitani_maxprice,
        value: ocitani_price,
        slide: function (event, ui) {
            // ovaj deo je po formuli originalnog autora slidera. treba zameniti vasim formulama
            amount = ui.value;
            if (amount <= limit) {
                traditional = amount * 0.12;
                traditional = traditional.toFixed(2);
                flat = 2995 + amount * 0.025;
                flat = flat.toFixed(2);
                savings = traditional - flat;
                savings = savings.toFixed(2);

                $("#amount").val(amount.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"));
                $("#traditional").val('$' + traditional.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"));
                $("#flat").val('$' + flat.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"));
                $("#savings").val('$' + savings.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"));
                $(".blue").css("width", amount / 100000 + "%");

            } else if (amount > limit) {
                traditional = amount * 0.12;
                traditional = traditional.toFixed(2);
                flat = 2995 + amount * 0.01;
                flat = flat.toFixed(2);
                savings = traditional - flat;
                savings = savings.toFixed(2);

                $("#amount").val(amount.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"));
                $("#traditional").val('$' + traditional.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"));
                $("#flat").val('$' + flat.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"));
                $("#savings").val('$' + savings.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"));
                $(".blue").css("width", amount / 100000 + "%");
                
            }
        }
    });

    $(".ui-slider-handle").append("<input type='text' id='amount' value='" + ocitani_price + "'>");
    /* end of slider script*/
})(jQuery);

