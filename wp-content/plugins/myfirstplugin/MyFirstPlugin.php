<?php
/*
 * Plugin Name: MyFirstPlugin
 * Plugin URI: http://127.0.0.1/
 * Description: First Plugin-this info will be displayed in plugin page
 * Version: 1.0.0
 * Author: Dragana Poznan
 * Author URI: http://127.0.0.1/
 * 
 */

add_action('admin_menu', 'myfirstplugin_admin_actions');

function myfirstplugin_admin_actions() {
    add_options_page('MyFirstPlugin', 'MyFirstPlugin', 'manage_options', __FILE__, 'myfirstplugin_admin');
}

function myfirstplugin_admin() {
    ?>
    <div class="wrap">
        <h2>A More Interesting Hello World Plugin</h2>
        <h2>This plugin will search the DB for all draft posts and display their Title and ID</h2>
        <p>Click the button below to begin the search</p>
        <br/>
        <form action="" method="post">
            <input type="submit" name="search_draft_posts" value="Search" class="button-primary"/>
        </form>
        <br/>

        <table class="widefat">
            <thead>
                <tr>
                    <th>Post Title</th>
                    <th>Post ID</th>
                </tr>
            </thead>
            <tfoot>
                <tr>
                    <th>Post Title</th>
                    <th>Post ID</th>
                </tr>
            </tfoot>
            <tbody>
                <?php
                global $wpdb;

                $mytestdrafts = $wpdb->get_results(
                        "SELECT ID, post_title FROM $wpdb->posts WHERE post_status = 'publish'"
                );

                foreach ($mytestdrafts as $mytestdraft) {
                    ?>
                    <tr>
                        <?php
                        echo "<td>" . $mytestdraft->post_title . "</td>";
                        echo "<td>" . $mytestdraft->ID . "</td>";
                    }
                    ?>
                </tr>

            </tbody>
        </table>
    </div>
    <?php
}
