<?php
/**
 *	Calcium WordPress Theme
 *	
 *	Laborator.co
 *	www.laborator.co 
 */


// After theme setup hooks
function calcium_child_after_setup_theme() {
	// Load translations for child theme
	load_child_theme_textdomain( 'calcium', get_stylesheet_directory() . '/languages' );
}

add_action( 'after_setup_theme', 'calcium_child_after_setup_theme' );

// This will enqueue style.css of child theme
function calcium_child_wp_enqueue_scripts() {
	wp_enqueue_style( 'calcium-child', get_stylesheet_directory_uri() . '/style.css' );
}

add_action( 'wp_enqueue_scripts', 'calcium_child_wp_enqueue_scripts', 100 );