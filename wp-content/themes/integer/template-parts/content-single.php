<?php
/**
 * The template used for displaying post content in single.php.
 *
 * @package Integer
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

    <?php integer_entry_meta_before_title(); ?>

    <header class="entry-header">

        <?php
        the_title('<h1 class="entry-title">', '</h1>');
        $neki_price = get_post_meta($post->ID, $key = 'price', true);
        ?>
        <div>
            bla bla bla ispis custom polja : <?php echo $neki_price; ?>
        </div>
        <div id="parent-fee-slider" data-field-price="<?php echo $neki_price; ?>">
            ovde slider<br>price: <?php echo $neki_price; ?><br>
            <?php
            pozdrav(); // OBICNO znaci vidi funkciju kao gflobal ali promenjive ne
            if (class_exists('MysliderPrice')) {
                ?>
                hej           hej hej11111111111
                <?php
            } else {
                ?>
                no no no
                <?php
            }
            $mysliderpriceb = new MysliderPrice();
            $mysliderpriceb->register();
            ?><br><br>
            hej hej hej22222222
            <br><br>
            <?php $mysliderpriceb->pozdrav(); ?><br> <br><br>

            <?php
            global $mysliderprice;
            $mysliderprice->pozdrav();
            $mysliderprice->render();
            ?>

        </div>

    </header><!-- .entry-header -->

<?php integer_entry_meta_before_content(); ?>

    <?php integer_post_thumbnail(); ?>

    <div class="entry-content">

<?php the_content(); ?>

        <?php
        wp_link_pages(array(
            'before' => '<div class="page-links">' . __('Pages:', 'integer'),
            'after' => '</div>',
        ));
        ?>

    </div><!-- .entry-content -->

<?php integer_entry_footer_widgets(); ?>

    <?php integer_entry_meta_after_content(); ?>

</article><!-- #post-## -->
