<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme and one
 * of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query,
 * e.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
get_header();
?>
<!-- ovde se resava funkcionanost home page stranice-->
<div id="main-content" class="main-content">

    <?php
    if (is_front_page() && twentyfourteen_has_featured_posts()) {
        // Include the featured content template.
        get_template_part('featured-content');
    }
    ?>

    <div id="primary" class="content-area">
        <div id="content" class="site-content" role="main">

            <?php
            if (have_posts()) :
                // Start the Loop.
                while (have_posts()) : the_post();

                    /*
                     * Include the post format-specific template for the content. If you want to
                     * use this in a child theme, then include a file called called content-___.php
                     * (where ___ is the post format) and that will be used instead.
                     */
                    get_template_part('content', get_post_format());

                endwhile;
                // Previous/next post navigation.
                twentyfourteen_paging_nav();

            else :
                // If no content, include the "No posts found" template.
                get_template_part('content', 'none');

            endif;
            ?>
            <!-- new-menu je iz fajla funkcije linija koda 26-->
            <!-- dodavanje novog menija-->
            <!-- u array-u new-menu je id koji ce se pokazati svuda u admin delu po id-u se zna koji je koji menu-->
            <?php wp_nav_menu(array('theme_location' => 'new-menu'));?>
            <!-- dodvanje widget-a-->
            <?php if (dynamic_sidebar('level_up_new_widget_area')) : else : endif; ?>
            <!-- ukoliko postoji content u widget area ispisace ga, u argumentu ide string ime widget area ali se ovde misli na id, uzeto iz fajla functions.php linija 14-->
        </div><!-- #content -->
    </div><!-- #primary -->
    <?php get_sidebar('content'); ?>
</div><!-- #main-content -->

<?php
get_sidebar();
get_footer();
