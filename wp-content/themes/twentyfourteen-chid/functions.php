<?php

function twentyfourteen_child_scripts(){
    wp_enqueue_script('name of script', get_stylesheet_directory_uri() . '/js/extra.js');
}

//adding a hook gleda neku drugu akciju koja je def
//ovde se dodaje javascript funkcionalnost
add_action('wp_enqueue_scripts', 'twentyfourteen_child_scripts');

function twentyfourteen_child_widgets_init(){
    
    register_sidebar(array(
        'name' => 'Level Up New Widget Area',
        'id' => 'level_up_new_widget_area',
        'before_widget' => '<aside>',
        'after_widget' => '</aside>',
        'before_title' => '<h3 class="widget-title">',
        'after-title' => '</h3>',
    ));
    
}
//add funcionality , first art $tag gde hocemo da zakacimo funkciju , drugi argument naziv funkcije
//ovde se dodaje widget
add_action('widgets_init', 'twentyfourteen_child_widgets_init');

function twentyfourteen_child_register_menu(){
    //prvi argument je naziv menija kako zelimo da bude registrovan, drugi 
    //argument predstavlja kako ce se zvati taj meni u aplikaciji tj wordpressu
    register_nav_menu('new-menu', __('Our New Menu'));
}

//ovde se dodaje funkcionalnost za wordpress generalno u ovom slucaju za menu
add_action('init', 'twentyfourteen_child_register_menu');

add_filter('pre_option_link_manager_enabled', '__return_true');
