<div class="row">
	<div class="contact-page">
		<div class="large-6 medium-6 columns contact-address">
			<p>
				<?php echo laborator_esc_script( nl2br(get_field('address')) ); ?>
			</p>			
		</div>
		
		<div class="large-6 medium-6 columns contact-information">
			<p>
				<?php echo laborator_esc_script( nl2br(get_field('phone_and_email')) ); ?>
			</p>
		</div>
	</div>
</div>