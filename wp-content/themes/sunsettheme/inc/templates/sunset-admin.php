<!-- OVA STRANICA JE IZGLED CELE STRANICE U ADMINU KOJU SMO NAPRAVILI -->
<h1>Sunset Sidebar Options</h1>
<h3 class="title">Manage Options</h3>
<p>Costomize Sidebar Options</p>
<?php //bloginfo('name'); //ispisace sajt title?>
<?php settings_errors(); ?>
<?php
$picture = esc_attr(get_option('profile_picture'));
$firstName = esc_attr(get_option('first_name'));
$lastName = esc_attr(get_option('last_name'));
$fullName = $firstName . ' ' . $lastName;
$description = esc_attr(get_option('user_description'));
?>

<div class="sunset-sidear-preview">
    <div class="sunset-sidebar">
        <div class="image-container">
            <div id="profile-picture-preview" class="profile-picture" style="background-image: url(<?php print $picture;?>);">
<!--                <img src="<?php //print $picture; ?>"/>-->
            </div>
        </div>
        <h1 class="sunset-username"><?php print $fullName; ?></h1>
        <h2 class="sunset-description"><?php print $description; ?></h2>
        <div class="icon-wrapper">


        </div>
    </div>
</div>
<!--uvek se koristi post metod-->
<form class="sunset-general-form" method="post" action="options.php">
    <!-- funkcija sunset-settings-group je iz fajla function-admin.php linija koda 49-->
    <?php settings_fields('sunset-settings-group'); ?>
    <!-- ovako na stranici ubacujemo funkcionalnost iz definisane sekcije u fajlu funciton-admin.php -->
    <!-- unutar funkcije definisemo ime page gde section pripada-->
    <?php do_settings_sections('urlslug_sunset'); ?>
    <?php submit_button(); ?>
</form>