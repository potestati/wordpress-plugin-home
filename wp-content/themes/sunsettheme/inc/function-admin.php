<?php

/*
  @package sunsettheme
 * 
 * ===========================
 *         ADMIN PAGE
 * ===========================
 * 
 *  */

//TEMPLATE SUBMENU FUNCTIONS


function sunset_add_admin_page() {
    /* funkcija dodaje opciju u admin menu , poslednji el predstavlja poziciju u admin meniju */

//GENERATE SUNSET ADMIN PAGE
    //
    //prvi parametar je title koji ce se pojaviti na page kada kliknemo na admin sidebar dugme
    //drugi title je naziv dugmeta na admin menniju 
    //manage_options su skoro sve opcije dodavanje, upravljanje, brisanje itd
    //menu_slug je link url u admin delu teme ovde
    //sledeci parametar je funkcija u stringu
    //sledeci parametar je ikonica koja ce se prikazivati u admin delu kod sunset dugmeta
    //poslednji parametar je pozicija menu taba u admin delu , pogle legendu u skripti rasporeda tabova u admin delu
    add_menu_page('Sunset Theme Options', 'Sunset', 'manage_options', 'urlslug_sunset', 'sunset_theme_create_page', get_template_directory_uri() . '/img/sunset-icon.png', 110);

//GENERATE SUNSET ADMIN SUB PAGES
    /* ova funkcija generise submenu page koji je vezan za urlslug_sunset prvo ide slug, naziv stranice, sta radi, opcije koje dozvoljavaju sve
      url slug bas te stranice , naziv funkcije koja generise stranicu i obezbedjuje funkcionalnost
     *      */
    //od url slug zavisi da li ce se ispisivati sunset ili odmah general u admin meniju
    add_submenu_page('urlslug_sunset', 'Sunset Sidebar Options', 'Sidebar', 'manage_options', 'urlslug_sunset', 'sunset_theme_create_page');
    add_submenu_page('urlslug_sunset', 'Sunset Theme Options', 'Settings', 'manage_options', 'urlslug_sunset_settings', 'sunset_theme_create_page');
    add_submenu_page('urlslug_sunset', 'Sunset Theme Options', 'Theme Options', 'manage_options', 'urlslug_sunset_theme', 'sunset_theme_suport_page');
    add_submenu_page('urlslug_sunset', 'Sunset CSS Options', 'Custom CSS', 'manage_options', 'urlslug_sunset_css', 'sunset_theme_settings_page');

//ACTIVATE CUSTOM SETTINGS - admin_init inicijalizacija admin section
    add_action('admin_init', 'sunset_custom_settings');
}

//DODAVANJE DEFINISANIH STRANICA U ADMIN MENU
//naziv akcije je admin_menu zove se hook
add_action('admin_menu', 'sunset_add_admin_page');//OVO SMO STAVILI VAN FUNKCIJE KOJA SE OVDE POZIVA I ADD ACTION DODAJE AKCIJU U ADMIN U SMISLU DODAJE NOVI PAGE


//DEFINISANJE POLJA ZA FORMU KOJA SU U ADMIN STRANICI I SLUZE ZA UNOS PODATAA
//JEDNA OD TIH POLJA JE I HIDDEN FIELD ZA INPUT SLIKE
function sunset_custom_settings() {
//SIDEBAR OPTIONS    
    //prvi argument je konekcija sa tabelom u bazi tj tacno odredjeno polje u bazi
    ///option name drugi parametar je single option da sacuvamo meni ili nesot sto hocemo
    //posle toga mozemo od te opcije da updejtujemo nesto ili da pozovemo vrednosst...
    register_setting('sunset-settings-group', 'profile_picture');
    register_setting('sunset-settings-group', 'first_name');
    register_setting('sunset-settings-group', 'last_name');
    register_setting('sunset-settings-group', 'user_description');
    register_setting('sunset-settings-group', 'twitter_handler','sunset_sanitize_twitter_handler');
    register_setting('sunset-settings-group', 'facebook_handler');
    register_setting('sunset-settings-group', 'gplus_handler');
    
    //DODAVANJE SEKCIJE NA STRANICU
    //ova funkcija ce da sacuva vrednosti iz imput fields, drugi je naziv , trei je funkcija koja obavlja stvari, poslednje je stranica gde zelimo da se sva polja isprintaju
    //prvi paranetar u funkciji je id section-a drugi je naziv koji section , treci funkcija , cetvrti id stranice kojoj pripada sekcija
    add_settings_section('sunset-sidebar-options', 'Sidebar Options', 'sunset_sidebar_options', 'urlslug_sunset');
    //dodaje polje u formi
    add_settings_field('sidebar-profile-picture', 'Profile Picture', 'sunset_sidebar_profile', 'urlslug_sunset', 'sunset-sidebar-options');//posednji parametar je options id, predhodni je page id
    add_settings_field('sidebar-name', 'Full Name', 'sunset_sidebar_name', 'urlslug_sunset', 'sunset-sidebar-options');
    add_settings_field('sidebar-description', 'Description', 'sunset_sidebar_description', 'urlslug_sunset', 'sunset-sidebar-options');
    add_settings_field('sidebar-twitter', 'Twitter handler', 'sunset_sidebar_twitter', 'urlslug_sunset', 'sunset-sidebar-options');
    add_settings_field('sidebar-facebook', 'Facebook handler', 'sunset_sidebar_facebook', 'urlslug_sunset', 'sunset-sidebar-options');
    add_settings_field('sidebar-gplus', 'Google+ handler', 'sunset_sidebar_gplus', 'urlslug_sunset', 'sunset-sidebar-options');
    
//THEME SUPPORT
}
// FUNKCIJE ZA STRANICE KOJE PRAVIMO
//INCLUDE FILE
function sunset_theme_create_page() {
    //pozivam fajl koji ce stilizovati stranicu na koju ova funkcija utice
    //get_template_directory() definise lokaciju
    require_once(get_template_directory() . '/inc/templates/sunset-admin.php');
}
// 
function sunset_theme_suport_page() {
    require_once(get_template_directory() . '/inc/templates/sunset-theme-support.php');
}
//ISPIS U SEKCIJI U DELU STRANICE
function sunset_sidebar_options() {
    echo 'Customaze your Sidebar Information';
}
//ISPIS NA TOJ STRANICI CUSTOM CSS
function sunset_theme_settings_page() {
    echo '<h1>Sunset Custom CSS</h1>';
}
//FUNKCIJE KOJE DEFINISU POLJA NA PAGE

//FUNKCIJA ZA UNOS SLIKE
/* Functions for registered fields in register_setting in sunset_custom_settings */
function sunset_sidebar_profile() {
    //ovaj red ispod ce sacuvati podatak iz input olja u bazi first_name je naziv polja iz funkcije register_setting
    $picture = esc_attr(get_option('profile_picture'));
    //kreiracemo button za upload slike , treba ce nam id kako  bi se bindovala slika sa js
    echo '<input class="button button-secondary" type="button" value="Upload Profile Picture" id="upload-button"><input id="profile-picture" type="hidden" name="profile_picture" value="' . $picture . '">';
//    button class je wp css klasa
}
//FUNKCIJE ZA UNOS IMENA I OSTALIH VREDNOSTI IZ POLJA FORME
//EXCAPE_ATTR JE FUNKCIJA KOJA CISTI OD SPECIJALNIH KARAKTERA UNOS PODATAKA
function sunset_sidebar_name() {
    //ovaj red ispod ce sacuvati podatak iz input olja u bazi first_name je naziv polja iz funkcije register_setting
    $firstName = esc_attr(get_option('first_name'));
    $lastName = esc_attr(get_option('last_name'));
    echo '<input type="text" name="first_name" value="' . $firstName . '" placeholder="First Name"><input type="text" name="last_name" value="' . $lastName . '" placeholder="Last Name">';
}
function sunset_sidebar_description() {
    $description = esc_attr(get_option('user_description'));
    echo '<input type="text" name="user_description" value="' . $description . '" placeholder="Description">';
}
function sunset_sidebar_twitter() {
    $twitter = esc_attr(get_option('twitter_handler'));
    echo '<input type="text" name="twitter_handler" value="' . $twitter . '" placeholder="Twitter handler">';
    echo '<p class="description">Input your Twitter username without the @ character.</p>';
}
function sunset_sidebar_facebook() {
    $facebook = esc_attr(get_option('facebook_handler'));
    echo '<input type="text" name="facebook_handler" value="' . $facebook . '" placeholder="Febook handler">';
}
function sunset_sidebar_gplus() {
    $gplus = esc_attr(get_option('gplus_handler'));
    echo '<input type="text" name="gplus_handler" value="' . $gplus . '" placeholder="Google+ handler">';
}

//Sanitization settings
//FUNKCIJA IZ POLJA ZA TWITTER CISTI DEF SPECIJALAN KARAKTER @
function sunset_sanitize_twitter_handler($input){
    $output = sanitize_text_field($input);
    $output = str_replace('@', '', $output);
    return $output;
}