<?php
/* enqueue - Registers the script if $src provided (does NOT overwrite), and enqueues it.
OVA FUNKCIJA JE DEFINISANA KAKO BI DODALA JS FILE CSS FILE I MEDIA UPDATE PO HOOK sunset_admin PROGRAM ZNA GDE DA UKLJUCI NA OJU STRANICU OVE FAJLOVE
 *  */
/*
  @package sunsettheme
 * 
 * ====================================
 *         ADMIN ENQUEUE FUNCTIONS
 * ====================================
 * 
 *  */

//ukoliko je stranica razlicita od toplevel_page_urlslug_sunset onda css file koji je includovan nece raditi
function sunset_load_admin_scripts($hook){
    
    if('toplevel_page_urlslug_sunset' != $hook){
        //vraticemo return varijablu i prekinucemo rad funkcije na taj nacin
        //empty return ustvari blokira izvrsenje funkcijes
        return;
    }
    
    //ove dve funkcije ispod se koriste da bi se registrovao css fajl i mogao da se koristi u buducnosti
    wp_register_style('sunset_admin', get_template_directory_uri() . '/css/sunset.admin.css', array(), '1.0.0', 'all');
    wp_enqueue_style('sunset_admin');

    //ova funkcija dodaje mogucnost da se koristi media u wordpressu koja dodaje slike i sl.
    wp_enqueue_media();
    
    //ove dve funkcije ukljucuju javascript file
    //prvo polje hook naziv skripte koji se registruje, drugi url adresa do fajla koji sadrzi skript
    //array('jquery') ovo poziva jquery ucitan u headeru wordpressa, sledeci parametar je verzija teme i poslednjji pita da li da ukljucis footer 
    wp_register_script('sunset-admin-script', get_template_directory_uri() . '/js/sunset.admin.js', array('jquery'), '1.0.0', true);
    wp_enqueue_script('sunset-admin-script');
}
//OVDE JE DODATA AKCIJA KOJU DEFINISE FUNKCIJA sunset_load_admin_scripts
add_action('admin_enqueue_scripts', 'sunset_load_admin_scripts');