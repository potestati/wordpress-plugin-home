//$ znak u funkciji omogucava da se iz funkcije pozove bilo koja jquery funkcija
jQuery(document).ready(function($){
    var mediaUploader;
    //znak $ poziva jquery
    //u e znak ce biti skupljene sve informacije koje proizilaze iz $('#upload-button')
    //destinacija buttona , id buttona ime itd.
    $('#upload-button').on('click', function(e){
        //ova funkcija je prebuild js funkcija koja ce blokirati bilo kakvu akciju koja je povezana sa el koji ima id #upload-button u ovom slucaju
        //da bude izvrsena. ako je def na button submiting form npr to se nece izvrsiti
        e.preventDefault();
        if(mediaUploader){
            //otvorice se mediaUploader nakon cega cemo staviti return kako bi skripta stala
            mediaUploader.open();
            return;
        }
        //ovaj kod nam treba da pristupimo razlicitim fazama poziva media uploadera
        //pogledati ovu funkciju na wp codex
        mediaUploader = wp.media.frames.file_frame = wp.media({
            title: 'Choose a Profile Piture',
            button: {
                text: 'Chose Picture'
            },
            //posto zelimo da selektujemo samo jednu sliku stavicemo false na multiple key
            multiple: false
        });
        //select je binding funkcija koja ce selektovati sliku iz mediaUploader
        mediaUploader.on('select', function(){// na selektovanje slike ce da proradi funkcija
            //u var attachment ce se uzeti selection prvi koji je selektovano transform json format
            attachment = mediaUploader.state().get('selection').first().toJSON();
            //ispod cemo staviti u jquery klasu ili id od input field gde zelimo da se pojavi attacment url
            $('#profile-picture').val(attachment.url);
            //ovde ispod umesto vrednosti pristupamo css property - definisali smo background image
            //ovo ispod resava da se odmah nakon klika na dugme ucitava slika a ne nakon save changes
            $('#profile-picture-preview').css('background-image', 'url('+attachment.url+')');
        });
        mediaUploader.open();
    });
});


